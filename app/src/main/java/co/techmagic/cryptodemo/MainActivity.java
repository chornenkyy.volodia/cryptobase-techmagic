package co.techmagic.cryptodemo;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.techmagic.cryptodemo.R;

import co.techmagic.cryptobase.AesCryptoManager;
import co.techmagic.cryptobase.CryptoManager;
import co.techmagic.cryptobase.SecureKeyManager;

public class MainActivity extends AppCompatActivity {

    private CryptoManager cryptoManager;

    private EditText etDataValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final String dataExample = "C88PkUdtexOGm1IJ0hFa7HXxAQ5FJGQg";
        etDataValue = findViewById(R.id.etDataValue);
        etDataValue.setText(dataExample); // initial data to work with

        SecureKeyManager secureKeyManager = new DemoSecureKeyManager();
        cryptoManager = new AesCryptoManager(secureKeyManager);

        getTextView(R.id.tvKeyValue).setText(secureKeyManager.getKey());
        getTextView(R.id.tvVectorValue).setText(secureKeyManager.getInitializationVector());

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String data = etDataValue.getText().toString();
                String encrypt = cryptoManager.encrypt(data);
                String decrypt = cryptoManager.decrypt(encrypt, data.length());

                getTextView(R.id.tvEncryptedValue).setText(encrypt);
                getTextView(R.id.tvDecryptedValue).setText(decrypt);
            }
        });
    }

    private TextView getTextView(@IdRes int viewId) {
        return findViewById(viewId);
    }
}

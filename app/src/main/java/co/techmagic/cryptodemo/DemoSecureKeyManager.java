package co.techmagic.cryptodemo;

import co.techmagic.cryptobase.SecureKeyManager;

public class DemoSecureKeyManager implements SecureKeyManager {

    @Override
    public String getKey() {
        return "keyWithNiceSecur";
    }

    @Override
    public String getInitializationVector() {
        return "vecWithNiceSecur";
    }
}

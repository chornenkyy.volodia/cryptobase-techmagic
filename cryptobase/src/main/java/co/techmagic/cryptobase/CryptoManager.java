package co.techmagic.cryptobase;

public interface CryptoManager {

    String encrypt(String data);

    String decrypt(String data, int tokenLength);
}

package co.techmagic.cryptobase;

public interface SecureKeyManager {

    String getKey();

    String getInitializationVector();
}
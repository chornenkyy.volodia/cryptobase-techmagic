package co.techmagic.cryptobase;

import android.support.annotation.Nullable;
import android.util.Base64;
import android.util.Log;

import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class AesCryptoManager implements CryptoManager {

    private SecureKeyManager secureKeyManager;

    public AesCryptoManager(SecureKeyManager secureKeyManager) {
        this.secureKeyManager = secureKeyManager;
    }

    @Override
    public String encrypt(String data) {
        byte[] input = data.getBytes();
        String key = secureKeyManager.getKey();
        String iv = secureKeyManager.getInitializationVector();
        byte[] keyBytes = key.getBytes();
        byte[] ivBytes = iv.getBytes();
        byte[] encryptedBytes = processData(Cipher.ENCRYPT_MODE, input, keyBytes, ivBytes);
        return Base64.encodeToString(encryptedBytes, Base64.DEFAULT);
    }

    @Nullable
    @Override
    public String decrypt(String data, int tokenLength) {
        byte[] input = Base64.decode(data, Base64.DEFAULT);
        String key = secureKeyManager.getKey();
        String iv = secureKeyManager.getInitializationVector();
        byte[] keyBytes = key.getBytes();
        byte[] ivBytes = iv.getBytes();
        byte[] decryptedBytes = processData(Cipher.DECRYPT_MODE, input, keyBytes, ivBytes);
        return decryptedBytes == null ? null : new String(Arrays.copyOf(decryptedBytes, tokenLength));
    }

    @Nullable
    private byte[] processData(int mode, byte[] input, byte[] keyBytes, byte[] ivBytes) {
        byte[] processed = null;
        SecretKeySpec key = new SecretKeySpec(keyBytes, "AES");
        IvParameterSpec ivspec = new IvParameterSpec(ivBytes);
        try {
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(mode, key, ivspec);
            processed = new byte[cipher.getOutputSize(input.length)];
            int processedLength = cipher.update(input, 0, input.length, processed, 0);
            cipher.doFinal(processed, processedLength);
        } catch (Exception e) {
            Log.e(getClass().getName(), Log.getStackTraceString(e));
        }
        return processed;
    }

}